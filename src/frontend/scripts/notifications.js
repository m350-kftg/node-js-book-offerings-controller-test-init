const CLASS_ERROR = 'error';
const CLASS_INFO = 'info';
const CLASS_DO_NOT_DISPLAY = 'do-not-display';

function addInfoNotification(msg) {
  updateNotification(msg, CLASS_INFO);
}

function addErrorNotification(msg) {
  updateNotification(msg, CLASS_ERROR);
}

function clearNotification() {
  updateNotification('', CLASS_DO_NOT_DISPLAY);
}

function updateNotification(msg, notificationClass) {
  const elNotification = document.getElementById('notification');
  elNotification.className = '';
  elNotification.classList.add(notificationClass);
  elNotification.textContent = msg;
}

export { addErrorNotification, addInfoNotification, clearNotification };
