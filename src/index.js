import express from 'express';
import path, { dirname } from 'path';
import { fileURLToPath } from 'url';
import { router as offeringRouter } from './backend/offering/offering.routes.js';
import mongoose from 'mongoose';

const app = express();

mongoose.connect('mongodb://127.0.0.1/bookofferings');

const __dirname = dirname(fileURLToPath(import.meta.url));
app.use(express.static(path.join(__dirname, 'frontend')));

app.use(express.json());
app.use('/api/offerings', offeringRouter);

mongoose.connection.once('open', () => {
  console.log('Connected to MongoDB');
  app.listen(3001, () => {
    console.log('Server listens to http://localhost:3001');
  });
});
